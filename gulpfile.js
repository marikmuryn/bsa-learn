'use strict';

const gulp = require('gulp'),
    scss = require('gulp-sass'),
    autoprefixer = require('autoprefixer'),
    wiredep = require('wiredep').stream,
    notify = require('gulp-notify'),
    useref = require('gulp-useref'),
    clean = require('gulp-clean'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    csso = require('gulp-csso'),
    surge = require('gulp-surge'),
    spritesmith = require('gulp.spritesmith'),
    merge = require('merge-stream'),
    tinypng = require('gulp-tinypng-free'),
    svgmin   = require('gulp-svgmin'),
    changed  = require('gulp-changed'),
    browserSync = require('browser-sync').create(),
    mqpacker = require('css-mqpacker'),
    postcss = require('gulp-postcss'),
    postcssFlexbugs = require('postcss-flexbugs-fixes'),
    nunjucksRender = require('gulp-nunjucks-render');



// Connect bower files to HTML
gulp.task('bower', function () {
    return gulp.src('app/*.html')
        .pipe(wiredep({
            optional: 'configuration',
            goes: 'app/bower_components'
        }))
        .pipe(gulp.dest('./app/'));
});

// Reload browser
gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./app"
        },
        //		tunnel: true,
        host: 'localhost',
        port: 3002,
        notify: false
    });
    browserSync.watch('app/*.{html,php}').on('change', browserSync.reload);
    browserSync.watch('app/css/*.*').on('change', browserSync.reload);
    browserSync.watch('app/js/*.*').on('change', browserSync.reload);
    browserSync.watch('app/img/*.*').on('change', browserSync.reload);
    browserSync.watch('app/images/*.*').on('change', browserSync.reload);
});

let processors = [
    autoprefixer({
        grid: true,
        browsers: ['>0.1%'],
        cascade: false
    }),
    mqpacker({
        sort: sortMediaQueries
    }),
    postcssFlexbugs
];

function isMax(mq) {
    return /max-width/.test(mq);
}
function isMin(mq) {
    return /min-width/.test(mq);
}
function sortMediaQueries(a, b) {
     var A = a.replace(/\D/g, '');
     var B = b.replace(/\D/g, '');
    if (isMax(a) && isMax(b)) {
        return B - A;
    } else if (isMin(a) && isMin(b)) {
        return A - B;
    } else if (isMax(a) && isMin(b)) {
        return 1;
    } else if (isMin(a) && isMax(b)) {
        return -1;
    }
    return 1;
}

// Compile scss
gulp.task('styles', function () {
    return gulp.src('app/scss/*.scss')
    // .pipe(scss().on('error', scss.logError))
        .pipe(scss().on('error', function (err) {
            notify().write(err);
            this.emit('end');
        }))
        .pipe(postcss(processors))
        .pipe(gulp.dest('app/css'));
});

// Create sprite
gulp.task('sprite', function () {
    var spriteData = gulp.src('src/icons/*.{png,jpg,jpeg}')
        .pipe(spritesmith({
            padding: 5,
            imgName: 'sprite.png',
            cssName: 'sprite.css',
            cssVarMap: function (sprite) {
                var name = sprite.name.slice();
                if (name.lastIndexOf('_hover') !== -1) {
                    name = name.slice(0, -6) + ':hover';
                } else if (name.lastIndexOf('_active') !== -1) {
                    name = name.slice(0, -7) + '.active';
                }
                if (name.lastIndexOf('_after') !== -1) {
                    sprite.name = name + ':after';
                } else {
                    sprite.name = name + ':before';
                }
            },
            cssTemplate: 'template/sprite.css.handlebars'
        }));
    var imgStream = spriteData.img.pipe(gulp.dest('src/images')),
        cssStream = spriteData.css.pipe(gulp.dest('app/css'));
    return merge(imgStream, cssStream);
});


// Clean dist
gulp.task('clean', function () {
    return gulp.src('dist/', {
        read: false
    }).pipe(clean());
});

// Optimize images
gulp.task('tinyImg', function () {
    return gulp.src('src/**/*.{png,jpg,jpeg}')
        .pipe(tinypng())
        .pipe(gulp.dest('app'))
});

gulp.task('svgOptimize', function () {
    return gulp.src('src/svg/**/*.svg')
        .pipe(changed('app/svg'))
        .pipe(svgmin({
            js2svg: {
                pretty: true
            },
            plugins: [{
                removeDimensions: true
            }, {
                removeDesc: true
            }, {
                removeTitle: true
            }, {
                cleanupIDs: true
            }, {
                mergePaths: false
            }]
        }))
        .pipe(gulp.dest('app/svg'))
});

gulp.task('img', function () {
    return gulp.src('app/img/**/*.*')
        .pipe(gulp.dest('dist/img'))
});
gulp.task('images', function () {
    return gulp.src('app/images/**/*.*')
        .pipe(gulp.dest('dist/images'))
});
gulp.task('imgSvg', function () {
    return gulp.src('app/svg/**/*.svg')
        .pipe(gulp.dest('dist/svg'))
});
gulp.task('img', gulp.parallel('img', 'images', 'imgSvg'));


// Comprese js
gulp.task('minify', function () {
    return gulp.src('app/*.{html,php}')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', csso()))
        .pipe(gulp.dest('dist'));
});


// Add fonts to dist
gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts'));
});


// writing up the gulp nunjucks task
gulp.task('nunjucks', function () {
    console.log('Rendering nunjucks files..');
    return gulp.src('app/pages' + '/**/*.+(html|js|css)')
        .pipe(nunjucksRender({
            path: ['app/templates'],
            watch: true
        }))
        .pipe(gulp.dest('app'));
});


// Push to server
gulp.task('surge', function () {
    return surge({
        project: './dist',
        domain: 'mmv.bsa.surge.sh'
    });
});


gulp.task('build', gulp.series('clean', gulp.parallel('img', 'fonts', 'minify')));

gulp.task('dist', gulp.series('build', 'surge', function () {
    return notify().write({
        title: 'Gulp: Surge',
        message: 'The project is already on the surge'
    })
}));

gulp.task('watch', function () {
    gulp.watch('app/scss/*.scss', gulp.series('styles'));
    gulp.watch('src/icons/*.{png,jpg,jpeg}', gulp.series('sprite'));
    gulp.watch('src/img/**/*.{png,jpg,jpeg}', gulp.series('tinyImg'));
    gulp.watch('src/images/**/*.{png,jpg,jpeg}', gulp.series('tinyImg'));
    gulp.watch('src/svg/**/*.svg', gulp.series('svgOptimize'));
    gulp.watch('bower.json', gulp.series('bower'));
    gulp.watch('app/pages' + '/**/*.html', gulp.series('nunjucks', 'bower'));
    gulp.watch('app/templates' + '/**/*.html', gulp.series('nunjucks', 'bower'));
});

gulp.task('default', gulp.series('styles',  gulp.parallel('svgOptimize', 'tinyImg', 'watch', 'serve', 'nunjucks', 'bower')));

// gulp.task('default', gulp.series('styles', gulp.parallel('watch', 'serve', gulp.series('nunjucks', 'bower'))));
