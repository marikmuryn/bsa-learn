'use strict';

const gulp = require('gulp'),
	scss = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	wiredep = require('wiredep').stream,
	notify = require('gulp-notify'),
	useref = require('gulp-useref'),
	clean = require('gulp-clean'),
	gulpif = require('gulp-if'),
	uglify = require('gulp-uglify'),
	csso = require('gulp-csso'),
	surge = require('gulp-surge'),
	spritesmith = require('gulp.spritesmith'),
	merge = require('merge-stream'),
	imagemin = require('gulp-imagemin'),
	browserSync = require('browser-sync').create();



// Connect bower files to HTML
gulp.task('bower', function () {
	return gulp.src('app/*.{html,php}')
		.pipe(wiredep({
			optional: 'configuration',
			goes: 'app/bower_components'
		}))
		.pipe(gulp.dest('./app'));
});


// Reload browser
gulp.task('serve', function () {
	browserSync.init({
		server: {
			baseDir: "./app"
		},
		//		tunnel: true,
		host: 'localhost',
		port: 3002
	});
	browserSync.watch('app/*.{html,php}').on('change', browserSync.reload);
	browserSync.watch('app/css/*.*').on('change', browserSync.reload);
	browserSync.watch('app/js/*.*').on('change', browserSync.reload);
	browserSync.watch('app/img/*.*').on('change', browserSync.reload);
	browserSync.watch('app/images/*.*').on('change', browserSync.reload);
});

// Compile scss
gulp.task('styles', function () {
	return gulp.src('app/scss/*.scss')
		.pipe(scss().on('error', scss.logError))
		.pipe(autoprefixer({
			browsers: ['last 15 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('app/css'));
});

// Create sprite
gulp.task('sprite', function () {
	var spriteData = gulp.src('app/images/icons/*.{png,jpg,jpeg}')
		.pipe(spritesmith({
			retinaSrcFilter: 'app/images/icons/*@2x.png',
			imgName: 'sprite.png',
			retinaImgName: 'sprite@2x.png',
			cssName: 'sprite.css',
			cssVarMap: function (sprite) {
				var name = sprite.name.slice();
				if (name.lastIndexOf('_hover') !== -1) {
					sprite.name = name.replace('_hover',':hover');
				} else if (name.lastIndexOf('_active') !== -1) {
					sprite.name = name.replace('_active',':active');
				}
			},
			cssTemplate: 'template/sprite.css.handlebars'
		}));
	var imgStream = spriteData.img.pipe(gulp.dest('app/images')),
		cssStream = spriteData.css.pipe(gulp.dest('app/css'));
	return merge(imgStream, cssStream);
});

// Clean dist
gulp.task('clean', function () {
	return gulp.src('dist/', {
		read: false
	}).pipe(clean());
});

// Optimize images
gulp.task('imgs', function () {
	return gulp.src('app/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'))
});
gulp.task('images', function () {
	return gulp.src('app/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
});
gulp.task('img', gulp.parallel('imgs', 'images'));


// Comprese js
gulp.task('minify', function () {
	return gulp.src('app/*.{html,php}')
		.pipe(useref())
		.pipe(gulpif('*.js', uglify()))
		.pipe(gulpif('*.css', csso()))
		.pipe(gulp.dest('dist'));
});

// Add fonts to dist
gulp.task('fonts', function () {
	return gulp.src('app/fonts/**/*.*')
		.pipe(gulp.dest('dist/fonts'));
});






// Pust to server
gulp.task('surge', function () {
	return surge({
		project: './dist',
		domain: 'page33.surge.sh'
	});
});


gulp.task('build', gulp.series('clean', gulp.parallel('img', 'fonts', 'minify')));

gulp.task('dist', gulp.series('build', 'surge'));


gulp.task('watch', function () {
	gulp.watch('app/scss/*.scss', gulp.series('styles'));
	gulp.watch('app/images/icons/*.{png,jpg,jpeg}', gulp.series('sprite'));
	gulp.watch('bower.json', gulp.series('bower'));
});

gulp.task('default', gulp.series('styles', gulp.parallel('watch', 'serve')));